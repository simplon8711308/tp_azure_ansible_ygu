# TP01_Effectuer un ping vers toutes les machines avec Ansible   

Il faut d'abord créer et configurer un fichier "inventory.ini", en ajoutant tout les VMs qu'on veut pinger(IP ou FQDN). et puis, on peut pinger tous les VMs à l'aide de cet fichier.   

1. Placer dans le dossier "ansible_quickstart" créé précédamment.  
2. Créer un fichier "inventory.ini"  
```
ygu@Ansible-Master:~/ansible_quickstart$ sudo nano inventory.ini
```  
3. Ajouter une nouvelle groupe "myhosts" en précisante IP ou FQDN du "Node-web" et du "Node-db"   

inventory.ini  
```
[myhosts]
"10.0.12.20",
"10.0.12.36
```
4. Vérifier l'inventaire, en tapante :  
```
ansible-inventory -i inventory.ini --list
```
On a donc:   
```
{
    "_meta": {
        "hostvars": {}
    },
    "all": {
        "children": [
            "ungrouped",
            "myhosts"
        ]
    },
    "myhosts": {
        "hosts": [
            "10.0.12.20",
            "10.0.12.36"
        ]
    }
}
```
5. On peut donc ping tout les VMs dans [myhosts] avec la commande :
```
ansible myhosts -m ping -i inventory.ini
```
On vas voir le resultat de ping : 
```
10.0.12.36 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
10.0.12.20 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```
