# TP08_Créer une variable locale nommée port_http contenant la valeur 8080. Utiliser cette variable dans un message : Le port 8080 sera utilisé
# 1. L'inventaire
On utilise encore celle de TP03.  
inventory03.ini  
```
[all]
Node-web ansible_host=10.0.12.20 ansible_hostname=hostname1 disk_space_available=
Node-db ansible_host=10.0.12.36 ansible_hostname=hostname2 disk_space_available=
# Ajoutez d'autres machines si nécessaire

``` 
# 2. Le playbook
var_local_http8080.yml
```
---
- name: Utilisation d'une variable locale
  hosts: localhost
  vars: # déclaration de variable locale
    port_http: 8080
  tasks:
    - name: Afficher le message utilisant la variable locale
      debug:
        msg: "Le port {{ port_http }} sera utilisé"

```
# 3. Avec la commande suivant, on lance l'inventaire et le plabook:
```
ansible-playbook -i inventory03.ini var_local_http8080.yml
```
On a sur le terminal:
```
PLAY [Utilisation d'une variable locale] *************************************************************************************

TASK [Gathering Facts] *******************************************************************************************************
ok: [localhost]

TASK [Afficher le message utilisant la variable locale] **********************************************************************
ok: [localhost] => {
    "msg": "Le port 8080 sera utilisé"
}

PLAY RECAP *******************************************************************************************************************
localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```