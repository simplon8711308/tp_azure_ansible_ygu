# TP09_Utilisation d’une variable locale de type dictionnaire. Utiliser cette variable dans un  message : Le logiciel apache sera installé par le paquet httpd.  
# 1. L'invetaire  
On utilise encore celle de TP03.  
inventory03.ini  
```
[all]
Node-web ansible_host=10.0.12.20 ansible_hostname=hostname1 disk_space_available=
Node-db ansible_host=10.0.12.36 ansible_hostname=hostname2 disk_space_available=
# Ajoutez d'autres machines si nécessaire
```
 
# 2. Le playbook  
Attension : le contrôleur Ansible nomé "Ansible-Master" dont IP est 10.0.12.4 n'est pas dans cette inventaire.   
Cependant, le playbook est exécuté localement sur "Ansible-Master" où Ansible est installé et configuré.  
La ligne de code "hosts: localhost" sera toujours exécuté même si l'hôte Ansible-Master n'est pas spécifié dans l'inventaire.   

var_local_dict_apache.yml  
```
---
- name: Utilisation d'une variable locale de type dictionnaire
  hosts: localhost
  vars:
    packages:
      apache: httpd
  tasks:
    - name: Afficher le message utilisant la variable locale de type dictionnaire
      debug:
        msg: "Le logiciel {{ packages.apache }} sera installé par le paquet {{ packages.apache }}"

``` 
# 3. Avec la commande suivant, on lance l'inventaire et le plabook:
```
ansible-playbook -i inventory03.ini var_local_dict_apache.yml
```
On a sur le terminal:  
```
PLAY [Utilisation d'une variable locale de type dictionnaire] ****************************************************************

TASK [Gathering Facts] *******************************************************************************************************
ok: [localhost]

TASK [Afficher le message utilisant la variable locale de type dictionnaire] *************************************************
ok: [localhost] => {
    "msg": "Le logiciel httpd sera installé par le paquet httpd"
}

PLAY RECAP *******************************************************************************************************************
localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```
